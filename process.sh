#!/bin/bash

set -x

#Collect:

wget https://datahub.io/core/population/r/population.csv

#Clean

cat population.csv | grep 'Spain'| sed 's/,/\t/g' > spain.tsv

#Visualize

cat spain.tsv | gnuplot -p -e "set terminal dumb; plot '<cat' using 3:4"

