#!/bin/bash

# Instalaremos emacs, git, wget, grep, gnuplot

sudo apt-get install emacs
sudo apt-get install git
sudo apt-get install wget
sudo apt-get install grep
sudo apt-get install gnuplot


# Configura git con tu nombre y dirección de correo

git config --global user.name "Laura Alcaraz"
git config --global user.email "laaau.o3@hotmail.com"



